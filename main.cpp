#include <iostream>
#include "Gates.h"
#include "Simulation.h"

using namespace std;


Signal& doSth(Signal x)
{
	return x;
}

int main()
{
	Simulation sim;

	sim.createArray(0, INPUT);
	sim.createArray(2, GATE);
	sim.createArray(2, OUTPUT);
	sim.createInputs(NULL, 100);
	sim.addGate(0, XOR);
	sim.addGate(1, NOT);
	
	int *x = new int[2];
	x[0] = 1; x[1] = 2;
	int *y = new int[1];
	y[0] = 2;
	sim.connectGates(0, x);
	sim.connectGates(1, y);
	sim.connectOutputs(0, 1);
	sim.connectOutputs(1, 2);


	//sim.createArray(1, INPUT);
	//sim.createArray(3, GATE);
	//sim.createArray(2, OUTPUT);
	//int **tab;
	//tab = new int*[2];
	//int sim_num = 2;
	//for (int i = 0; i < sim_num; i++)
	//{
	//	tab[i] = new int[1];
	//}
	//tab[0][0] = 0;
	//tab[1][0] = 1;
	//sim.createInputs(tab, sim_num);

	//sim.addGate(0, XOR);
	//sim.addGate(1, XOR);
	//sim.addGate(2, NOT);

	//int *x = new int[2];
	//x[0] = 2; x[1] = 3;
	//int *y = new int[2];
	//y[0] = 1; y[1] = 4;
	//int *z = new int[1];
	//z[0] = 4;
	//sim.connectGates(0, x);
	//sim.connectGates(1, y);
	//sim.connectGates(2, z);

	//sim.connectOutputs(0, 2);
	//sim.connectOutputs(1, 4);

	sim.start();


	return 0;
}
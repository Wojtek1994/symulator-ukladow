#pragma once

#include "Gates.h"
#include <iostream>
#include <list>

using namespace std;

enum GATES {NAND, NOR, NOT, AND, OR, XOR, XNOR};
enum ARRAYS {INPUT, OUTPUT, GATE};

class Array
{
public:
	Gate** array;
	int size;

	void create(int);
	string toString();

	Gate** operator[](int);

	Array::Array(void);
	Array::~Array(void);
};

class Simulation
{
private:
	bool seq;
	Array input;
	Array gate;
	Array output;
	Array sequential_gates;
	int** input_matrix;
	int simulation_number;
	list <string> result_list;

public:
	bool createArray(int, ARRAYS);
	bool createInputs(int**, int);
	bool addGate(int, GATES, int = -1);
	bool connectGates(int, int* = NULL);
	bool connectOutputs(int, int);
	void setInputs(int);
	bool compareOutputs(string);

	void evaluateOutput();
	void evaluateNextStates();
	void checkSequential();
	void reset();
	void clear();
	void start();

	Simulation(void);
	~Simulation(void);
};


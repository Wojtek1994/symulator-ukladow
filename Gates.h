#pragma once

enum VALUE {ZERO, ONE, UNDEFINED};		// it represents possible values of signal;

class Signal
{
public:
	VALUE value;

	Signal(void);
	Signal(int);
	~Signal(void);

	char toString();

	void operator= (int);
	bool operator== (int);
	bool operator!= (int);
};

class Gate
{
private:
	bool flag;
	Signal state;
	Signal next_state;
	bool is_evaluated;
	bool sequential;
public:
	Signal out;
	int in_signals_number;
	Gate **in_signals;

	bool getEvaluation();
	bool getFlag();
	Signal& getState();
	Signal& getNextState();
	bool getSequential();

	void setEvaluation(bool);
	void setFlag(bool);
	void setState(int);
	void setSequential(bool);
	void setNextState(int);

	bool preEvaluate();
	void evaluateInSignals();
	void evaluate();

	void reset();
	void clear(bool = false);

	virtual bool tryEvaluate() = 0;
	virtual void quickEvaluate() = 0;

	Gate(void);
	~Gate(void);
};

class Information: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	Information(int);
	Information(void);
	~Information(void);
};

class Result: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();
	Result(void);
	~Result(void);
};

class Nand: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	Nand(int);
	~Nand(void);
};

class Nor: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	Nor(int);
	~Nor(void);
};

class Not: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	Not(void);
	~Not(void);
};

class And: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	And(int);
	~And(void);
};

class Or: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	Or(int);
	~Or(void);
};

class Xor: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	Xor(void);
	~Xor(void);
};

class Xnor: public Gate
{
public:
	virtual bool tryEvaluate();
	virtual void quickEvaluate();

	Xnor(void);
	~Xnor(void);
};


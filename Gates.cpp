#include "Gates.h"
#include <iostream>
using namespace std;


// klasa $$Signal
Signal::Signal(void)
	:value (UNDEFINED)
{
}

Signal::Signal(int v)
{
	if (v == 0 | v == 1)
	{
		(v == 0)? value = ZERO: value = ONE;
	}
	else
	{
		value = UNDEFINED;
	}
}


Signal::~Signal(void)
{
}

char Signal::toString()
{
	switch (value)
	{
	case ZERO:
		{
			return '0';
		}
	case ONE:
		{
			return '1';
		}
	case UNDEFINED:
		{
			return '2';
		}
	default:
		{
			return '3';
		}
	}
}

void Signal::operator= (int new_value)
{
	switch(new_value)
	{
	case 0:
		{
			value = ZERO;
			break;
		}
	case 1:
		{
			value = ONE;
			break;
		}
	default:
		{
			value = UNDEFINED;
		}
	}
	return;
}

bool Signal::operator== (int v)
{
	if (value == v)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Signal::operator!= (int v)
{
	if (value == v)
	{
		return false;
	}
	else
	{
		return true;
	}
}


//klasa $$Gate
Gate::Gate(void)
	:is_evaluated(false), flag(false), state(UNDEFINED), next_state(UNDEFINED), sequential(false)
{

}

Gate::~Gate(void)
{
	if (in_signals_number > 0)
	{
		delete [] in_signals;
	}
}

bool Gate::getEvaluation()
{
	return is_evaluated;
};

bool Gate::getFlag()
{
	return flag;
}

Signal& Gate::getState()
{
	return state;
}

bool Gate::getSequential()
{
	return sequential;
}

Signal& Gate::getNextState()
{
	return next_state;
}

void Gate::setEvaluation(bool b)
{
	is_evaluated = b;
}

void Gate::setFlag(bool b)
{
	flag = b;
}

void Gate::setState(int v)
{
	if (v == 0 | v == 1)
	{
		state = v;
	}
	else
	{
		state = UNDEFINED;
	}
}

void Gate::setSequential(bool b)
{
	sequential = b;
}

void Gate::setNextState(int v)
{
	if (v == 0 | v == 1)
	{
		next_state = v;
	}
	else
	{
		next_state = UNDEFINED;
	}
}

void Gate::reset()
{
	state = UNDEFINED;
	out = UNDEFINED;
	setEvaluation(false);
}

void Gate::clear(bool to_next_state)	// default false
{
	if (getSequential())
	{
		if (to_next_state)
		{
			next_state = out.value;
		}
		else
		{
			state = out.value;
		}
	}
	else
	{
		state = UNDEFINED;
	}
	out = UNDEFINED;
	setEvaluation(false);
}

void Gate::evaluateInSignals()
{
	for (int i = 0; i < in_signals_number; i++)
	{
		if (!in_signals[i]->getEvaluation())
		{
			in_signals[i]->evaluate();
		}
	}
}

bool Gate::preEvaluate()
{
	if (getFlag())
	{
		setSequential(true);
		(getState() == UNDEFINED)? out = 1 : out = state;
		setEvaluation(true);
		return true;
	}
	if (getEvaluation())
	{
		return true;
	}
	return false;
}

void Gate::evaluate()
{
	if (preEvaluate())
	{
		return;
	}
	if (tryEvaluate())
	{
		setEvaluation(true);
		return;
	}
	setFlag(true);
	evaluateInSignals();
	quickEvaluate();
	setEvaluation(true);
	setFlag(false);
}

//klasa $$Information
Information::Information(int v)
{
	if (v == 0 | v == 1)
	{
		out = v;
	}
	else
	{
		out = UNDEFINED;
		// throw
	}
	setEvaluation(true);
	in_signals_number = 0;
};

Information::Information()
{
	in_signals_number = 0;
}

Information::~Information()
{
};

bool Information::tryEvaluate()
{
	return true;
}

void Information::quickEvaluate()
{
	return;
};


//klasa $$Result
Result::Result(void)
{
	in_signals_number = 1;
	in_signals = new Gate*[1];
}

Result::~Result(void)
{

}

bool Result::tryEvaluate()
{
	if (in_signals[0]->getEvaluation())
	{
		out = in_signals[0]->out;
		return true;
	}
	else
	{
		return false;
	}
}

void Result::quickEvaluate()
{
	out = in_signals[0]->out;
}

//klasa $$Nand
Nand::Nand(int in_s)
{
	if (in_s < 2)
	{
		// throw
		exit(1);
	}
	else
	{
		in_signals_number = in_s;
		in_signals = new Gate*[in_s];
	}
}

Nand::~Nand(void)
{

}

bool Nand::tryEvaluate()
{
	bool defined = true;
	for (int i = 0; i < in_signals_number; i++)
	{
		if (!in_signals[i]->getEvaluation())
		{
			defined = false;
		}
		else
		{
			if (in_signals[i]->out == 0)
			{
				out = 1;
				return true;
			}
		}
	}
	if (defined)
	{
		out = 0;
		return true;
	}
	else
	{
		return false;
	}
}

void Nand::quickEvaluate()
{
	for (int i = 0; i < in_signals_number; i++)
	{
		if (in_signals[i]->out == 0)
		{
			out = 1;
			return;
		}
	}
	out = 0;
}


//klasa $$Nor
Nor::Nor(int in_s)
{
	if (in_s < 2)
	{
		// throw
		exit(1);
	}
	else
	{
		in_signals_number = in_s;
		in_signals = new Gate*[in_s];
	}
}

Nor::~Nor(void)
{

}

bool Nor::tryEvaluate()
{
	bool defined = true;
	for (int i = 0; i < in_signals_number; i++)
	{
		if (!in_signals[i]->getEvaluation())
		{
			defined = false;
		}
		else
		{
			if (in_signals[i]->out == 1)
			{
				out = 0;
				return true;
			}
		}
	}
	if (defined)
	{
		out = 1;
		return true;
	}
	else
	{
		return false;
	}
}

void Nor::quickEvaluate()
{
	for (int i = 0; i < in_signals_number; i++)
	{
		if (in_signals[i]->out == 1)
		{
			out = 0;
			return;
		}
	}
	out = 1;
}

//klasa $$Not
Not::Not(void)
{
	in_signals_number = 1;
	in_signals = new Gate*[1];
}

Not::~Not(void)
{

}

bool Not::tryEvaluate()
{
	if (!in_signals[0]->getEvaluation())
	{
		return false;
	}
	else
	{
		if (in_signals[0]->out == 0)
		{
			out = 1;
			return true;
		}
		else
		{
			out = 0;
			return true;
		}
	}
}

void Not::quickEvaluate()
{
	if (in_signals[0]->out == 0)
	{
		out = 1;
	}
	else
	{
		out = 0;
	}
}

//klasa $$And
And::And(int in_s)
{
	if (in_s < 2)
	{
		// throw
		exit(1);
	}
	else
	{
		in_signals_number = in_s;
		in_signals = new Gate*[in_s];
	}
}

And::~And(void)
{

}

bool And::tryEvaluate()
{
	bool defined = true;
	for (int i = 0; i < in_signals_number; i++)
	{
		if (!in_signals[i]->getEvaluation())
		{
			defined = false;
		}
		else
		{
			if (in_signals[i]->out == 0)
			{
				out = 0;
				return true;
			}
		}
	}
	if (defined)
	{
		out = 1;
		return true;
	}
	else
	{
		return false;
	}
}

void And::quickEvaluate()
{
	for (int i = 0; i < in_signals_number; i++)
	{
		if (in_signals[i]->out == 0)
		{
			out = 0;
			return;
		}
	}
	out = 1;
}

//klasa $$Or
Or::Or(int in_s)
{
	if (in_s < 2)
	{
		// throw
		exit(1);
	}
	else
	{
		in_signals_number = in_s;
		in_signals = new Gate*[in_s];
	}
}

Or::~Or(void)
{

}

bool Or::tryEvaluate()
{
	bool defined = true;
	for (int i = 0; i < in_signals_number; i++)
	{
		if (!in_signals[i]->getEvaluation())
		{
			defined = false;
		}
		else
		{
			if (in_signals[i]->out == 1)
			{
				out = 1;
				return true;
			}
		}
	}
	if (defined)
	{
		out = 0;
		return true;
	}
	else
	{
		return false;
	}
}

void Or::quickEvaluate()
{
	for (int i = 0; i < in_signals_number; i++)
	{
		if (in_signals[i]->out == 1)
		{
			out = 1;
			return;
		}
	}
	out = 0;
}

//klasa $$XOR
Xor::Xor(void)
{
	in_signals_number = 2;
	in_signals = new Gate*[2];
}

Xor::~Xor(void)
{
}

bool Xor::tryEvaluate()
{
	if (!in_signals[0]->getEvaluation() | !in_signals[1]->getEvaluation())
	{
		return false;
	}
	if (in_signals[0]->out.value + in_signals[1]->out.value == 1)
	{
		out = 1;
	}
	else
	{
		out = 0;
	}
	return true;
}

void Xor::quickEvaluate()
{
	if ((in_signals[0]->out.value + in_signals[1]->out.value == 1))
	{
		out = 1;
	}
	else
	{
		out = 0;
	}
}

//klasa $$Xnor
Xnor::Xnor(void)
{
	in_signals_number = 2;
	in_signals = new Gate*[2];
}

Xnor::~Xnor(void)
{

}

bool Xnor::tryEvaluate()
{
	if (!in_signals[0]->getEvaluation() | !in_signals[1]->getEvaluation())
	{
		return false;
	}
	if (in_signals[0]->out.value + in_signals[1]->out.value == 1)
	{
		out = 0;
	}
	else
	{
		out = 1;
	}
	return true;
}

void Xnor::quickEvaluate()
{
	if ((in_signals[0]->out.value + in_signals[1]->out.value == 1))
	{
		out = 0;
	}
	else
	{
		out = 1;
	}
}

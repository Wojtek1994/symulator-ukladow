#include "Simulation.h"
#include <string>
#include <iterator>

using namespace std;

Simulation::Simulation(void)
	:seq(false)
{
}


Simulation::~Simulation(void)
{
	result_list.clear();
	for (int i = 0; i < simulation_number; i++)
	{
		if (input.size > 0)
		{
			delete [] input_matrix[i];
		}
	}
	delete [] input_matrix;
}

bool Simulation::createArray(int size, ARRAYS array_number)
{
	switch (array_number)
	{
	case INPUT:
		{
			input.create(size);
			for (int i = 0; i < input.size; i++)
			{
				*input[i] = new Information();
			}
			return true;
		}
	case OUTPUT:
		{
			output.create(size);
			for (int i = 0; i < output.size; i++)
			{
				*output[i] = new Result();
			}
			return true;
		}
	case GATE:
		{
			gate.create(size);
			return true;
		}
	default:
		{
			return false;
		}
	}
}

bool Simulation::addGate(int index, GATES gate_name, int number_of_signals)
{
	if (index < 0 | index >= gate.size)
	{
		return false;
	}
	switch (gate_name)
	{
	case NAND:
		{
			*gate[index] = new Nand(number_of_signals);
			return true;
		}
	case NOR:
		{
			*gate[index] = new Nor(number_of_signals);
			return true;
		}
	case NOT:
		{
			*gate[index] = new Not();
			return true;
		}
	case AND:
		{
			*gate[index] = new And(number_of_signals);
			return true;
		}
	case OR:
		{
			*gate[index] = new Or(number_of_signals);
			return true;
		}
	case XOR:
		{
			*gate[index] = new Xor();
			return true;
		}
	case XNOR:
		{
			*gate[index] = new Xnor();
			return true;
		}
	default:
		{
			return false;
		}
	}
}

bool Simulation::connectOutputs(int index, int signal_number)
{
	if (index >= 0 & index < output.size)
	{
		if (signal_number <= input.size)
		{
			(*output[index])->in_signals[0] = *input[signal_number - 1];
		}
		else
		{
			if (signal_number > input.size + gate.size)
			{
				return false;
			}
			else
			{
				(*output[index])->in_signals[0] = *gate[signal_number - input.size - 1];
			}
		}
		return true;
	}
	return false;
}

bool Simulation::connectGates(int index, int *inputs)
{
	if (index >= 0 & index < gate.size)
	{
		for (int i = 0; i < (*gate[index])->in_signals_number; i++)
		{
			if (inputs[i] <= input.size)
			{
				(*gate[index])->in_signals[i] = *input[inputs[i] - 1];
			}
			else
			{
				if (inputs[i] > input.size + gate.size)
				{
					return false;
				}
				else
				{
					(*gate[index])->in_signals[i] = *gate[inputs[i] - input.size - 1];
				}
			}
		}
		return true;
	}
	return false;
}

bool Simulation::createInputs(int** matrix, int number_of_simulations)
{
	if (number_of_simulations < 0)
	{
		return false;
	}
	else
	{
		if (input.size == 0)
		{
			simulation_number = 1;
		}
		else
		{
			simulation_number = number_of_simulations;
		}
	}
	input_matrix = matrix;
	return true;
}

void Simulation::setInputs(int number)
{
	for (int i = 0; i < input.size; i++)
	{
		(*input[i])->out = input_matrix[number][i];
		(*input[i])->setEvaluation(true);
	}
}

void Simulation::reset()
{
	seq = false;
	for (int i = 0; i < gate.size; i++)
	{
		(*gate[i])->reset();
	}
	for (int i = 0; i < output.size; i++)
	{
		(*output[i])->reset();
	}
}

void Simulation::evaluateOutput()
{
	for (int i = 0; i < output.size; i++)
	{
		(*output[i])->evaluate();
	}
}

void Simulation::clear()
{
	for (int i = 0; i < gate.size; i++)
	{
		(*gate[i])->clear();
	}
	for (int i = 0; i < output.size; i++)
	{
		(*output[i])->clear();
	}
}

void Simulation::checkSequential()
{
	int counter = 0;
	for (int i = 0; i < gate.size; i++)
	{
		if ((*gate[i])->getSequential())
		{
			seq = true;
			counter++;
		}
	}
	sequential_gates.create(counter);
	counter = 0;
	for (int i = 0; i < gate.size; i++)
	{
		if ((*gate[i])->getSequential())
		{
			*sequential_gates[counter] = *gate[i];
			counter++;
		}
	}
}

void Simulation::evaluateNextStates()
{
	for (int i = 0; i < sequential_gates.size; i++)
	{
		for (int j = 0; j < sequential_gates.size; j++)
		{
			if (j != i)
			{
				(*sequential_gates[j])->out = (*sequential_gates[j])->getState();
				(*sequential_gates[j])->setEvaluation(true);
			}
		}
		(*sequential_gates[i])->evaluate();
		for (int j = 0; j < sequential_gates.size; j++)
		{
			(*sequential_gates[j])->clear(j == i);
		}
	}
	for (int i = 0; i < sequential_gates.size; i++)
	{
		(*sequential_gates[i])->out = (*sequential_gates[i])->getNextState();
		(*sequential_gates[i])->setEvaluation(true);
	}
}

bool Simulation::compareOutputs(string last_value)
{
	list<string>::iterator list_end = result_list.end();
	list_end--;
	list<string>::iterator iter = result_list.begin();
	while (iter != list_end)
	{
		if (iter->compare(last_value) == 0)
		{
			return true;
		}
		iter++;
	}
	return false;
}

void Simulation::start()
{
	string result;
	for (int i = 0; i < simulation_number; i++)
	{
		reset();
		result_list.clear();
		setInputs(i);
		evaluateOutput();
		checkSequential();
		result_list.push_back(sequential_gates.toString().append(output.toString()));
		cout << "Symulacja numer: " << i + 1 << endl << sequential_gates.toString().append(output.toString()) << endl;
		clear();
		do
		{
			if (seq)
			{
				evaluateNextStates();
			}
			evaluateOutput();
			result = sequential_gates.toString().append(output.toString());
			result_list.push_back(result);
			cout << result << endl;
			clear();
		}
		while (!compareOutputs(result));
		// send outputs to data_writer
	}
}

Array::Array(void)
	:size(0)
{
}

Array::~Array()
{
	for (int i = 0; i < size; i++)
	{
		if (array[i]->getSequential())
		{
			array[i]->setSequential(false);
		}
		else
		{
			delete array[i];
		}
	}
	delete [] array;
}

Gate** Array::operator[](int index)
{
	if (index >= 0 & index < size)
	{
		return &array[index];
	}
	else
	{
		//throw
		exit(1);
	}
}

void Array::create(int s)
{
	if (s < 0)
	{
		//throw
		exit(1);
	}
	else
	{
		size = s;
		array = new Gate*[size];
	}
}


string Array::toString()
{
	string s = "";
	for (int i = 0; i < size; i++)
	{
		s += array[i]->out.toString();
	}
	return s;
}

